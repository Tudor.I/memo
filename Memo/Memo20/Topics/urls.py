from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.TopicsView.as_view(), name="topics"),
    path('topics/create', views.TopicsCreate.as_view(), name="topic-create"),
    path('topics/view-upd', views.TopicsViewUp.as_view(), name='topics-view-user')

]
