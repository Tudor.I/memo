from django.db import models
from Users.models import *


# Create your models here

class Topic(models.Model):
    name = models.CharField(max_length=70, blank=True, null=True)
    brief_description = models.TextField(max_length=500, blank=True, null=True)
    user_id = models.ForeignKey(to=CustomUser, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name
