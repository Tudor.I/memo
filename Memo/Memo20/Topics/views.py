from django.shortcuts import render
import datetime
import functools

import django
import json

from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.http import HttpResponse

from django.db.models import Q
from django.conf import settings

from .models import *
from .forms import *


# Create your views here.

class TopicsView(ListView):
    template_name = "topicss/topic_view.html"

    def get(self, request, *args, **kwargs):
        user = CustomUser.objects.filter(pk=request.user.id)
        user = list(user)
        if len(user) == 1:
            user = list(user)[0]
        else:
            user = None
        topics = Topic.objects.filter(user_id=user)
        return render(request, "topicss/topic_view.html", {"topics": topics, 'len': len(topics), "user": user})


class TopicsCreate(CreateView):
    model = Topic
    template_name = 'topicss/topic_create.html'
    context_object_name = "form"
    fields = "__all__"
    success_url = reverse_lazy("topics")

    def get(self, request, *args, **kwargs):
        form = CreateTopicForm()
        return render(request, 'topicss/topic_create.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            Topic.objects.create(name=form['name'].data,
                                 brief_description=form['brief_description'].data,
                                 user_id=request.user).save()
            return redirect("topics")
        return redirect("topic-create")


class TopicsViewUp(ListView):
    template_name = "topicss/topics_upd_view.html"
    model = Topic
    context_object_name = "topics"
