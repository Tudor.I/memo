from .models import *
from Topics.models import *
from django import forms


class CreateTopicForm(forms.ModelForm):
    name = forms.CharField(max_length=70)
    brief_description = forms.CharField(widget=forms.Textarea, max_length=5000)

    class Meta:
        model = Topic
        fields = [
            "name",
            "brief_description"
        ]
