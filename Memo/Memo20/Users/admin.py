from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *
from .forms import *
# Register your models here.

class AdminUser(UserAdmin):
    add_form = SignUpForm
    form = ProfileForm
    model = CustomUser
    list_display = ['username', 'profile_img', 'notes']

admin.site.register(CustomUser, AdminUser)

