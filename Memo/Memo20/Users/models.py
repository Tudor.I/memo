from django.db import models

# Create your models here.
from django.conf import settings
from django.contrib.auth.models import AbstractUser, User


class CustomUser(AbstractUser):
    profile_img = models.ImageField(upload_to='gallery', blank=True, null=True, default="profiledef.png")
    email = models.EmailField(max_length=100, blank=True, null=True)
    notes = models.TextField(max_length=1000, blank=True, null=True)

    def __str__(self):
        return self.username

