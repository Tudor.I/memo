
from django.shortcuts import render
import datetime
import functools

import django
import json

from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.http import HttpResponse

from django.db.models import Q
from django.conf import settings

from .models import *
from .forms import *


# Create your views here.

# CRUD for USER

class ProfileCreate(CreateView):
    form_class = SignUpForm
    template_name = "users/profile_register.html"
    success_url = reverse_lazy('login')


class ProfileView(ListView, LoginRequiredMixin):
    model = CustomUser
    template_name = "users/profile_view.html"
    context_object_name = "user"

    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('id')
        prof = CustomUser.objects.get(id=pk)
        return render(request, "users/profile_view.html", {'user': prof})


@login_required
def profile_update(request, *args, **kwargs):
    if request.method == "POST":
        frm = ProfileUpdateForm(request.POST or None)
        check_for_email = CustomUser.objects.filter(email=frm['email'].data)
        print(check_for_email)
        if len(check_for_email) > 1:
            pk = kwargs.get("pk")
            user = CustomUser.objects.get(pk=pk)
            form = ProfileUpdateForm(initial={
                'username': user.username,
                'profile_img': user.profile_img,
                'notes': user.notes,
                'email': user.email
            })
            message = "An user with this email already exists."
            return render(request, "users/profile_update.html", {'form': form, "error_message": message})
        print("am ajuns aici")
        if not frm.is_valid():
            print(frm['username'].data)
            user = CustomUser.objects.get(id=request.user.id)
            user.username = frm['username'].data
            user.profile_img = frm['profile_img'].data
            user.notes = frm['notes'].data
            user.email = frm['email'].data
            user.save()
            return redirect('profile-view', id=request.user.id)
    pk = kwargs.get("pk")
    user = CustomUser.objects.get(pk=pk)
    form = ProfileUpdateForm(initial={
        'username': user.username,
        'profile_img': user.profile_img,
        'notes': user.notes,
        "email": user.email
    })
    return render(request, "users/profile_update.html", {'form': form})


class ProfileDelete(DeleteView, LoginRequiredMixin):
    model = CustomUser
    template_name = "users/profile_delete.html"
    context_object_name = "user"
    success_url = reverse_lazy('topics')


class Logout(TemplateView, LoginRequiredMixin):
    template_name = "users/registration/logged_out.html"
