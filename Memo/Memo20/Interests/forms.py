from .models import *
from Topics.models import *
from django import forms


class LinkUpdateFrom(forms.ModelForm):
    title = forms.CharField(max_length=150)
    url = forms.URLField()
    description = forms.CharField(widget=forms.Textarea, max_length=5000)
    topic = forms.ModelChoiceField(queryset=Topic.objects.filter())

    class Meta:
        model = Interests
        fields = [
            'title',
            'url',
            'description',
            'topic'
        ]


class CreateLinkForm(forms.ModelForm):
    title = forms.CharField(max_length=150)
    url = forms.URLField()
    description = forms.CharField(widget=forms.Textarea, max_length=5000)
    topic = forms.ModelChoiceField(queryset=Topic.objects.filter())

    class Meta:
        model = Interests
        fields = [
            'title',
            'url',
            'description',
            'topic'
        ]

    def clean(self):
        super(CreateLinkForm, self).clean()
        topics = Topic.objects.filter()
        if len(topics) == 0:
            self.add_error('topic', 'You cannot create a link because there is no topic to reference to.'
                                    'Please create a topic first')
