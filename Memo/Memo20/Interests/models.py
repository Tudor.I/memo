from django.db import models
from django.apps import apps
import datetime
# Create your models here.
from django import utils
from Topics.models import Topic



class Interests(models.Model):
    title = models.CharField(max_length=150, blank=True, null=True)
    url = models.URLField()
    description = models.TextField(max_length=5000, blank=True, null=True)
    creation_date = models.DateTimeField(blank=True, null=True)
    update_time = models.DateTimeField(blank=True, null=True)
    topic = models.ForeignKey(to=Topic, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.title
