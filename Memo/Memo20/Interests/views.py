from django.shortcuts import render
import datetime
import functools

import django
import json

from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.http import HttpResponse

from django.db.models import Q
from django.conf import settings

from .models import *
from .forms import *
from .services import send_email


# Create your views here.

class InterestsView(TemplateView):
    template_name = "interestss/home.html"

    def get(self, request, *args, **kwargs):
        if request.session.get('searched') is not None:
            srch = request.session['searched']
            request.session['src'] = "var"
            request.session['search'] = 'var'
            del request.session['searched']
            return load_browser(request, srch)
        pk = self.kwargs.get("pk")
        topic = Topic.objects.get(pk=pk)
        page = request.GET.get('page')
        links = Interests.objects.filter(topic=topic)
        paginator = Paginator(links, 10)
        try:
            links = paginator.page(page)
        except PageNotAnInteger:
            links = paginator.page(1)
        except EmptyPage:
            links = paginator.page(paginator.num_pages)
        return render(request, 'interestss/home.html', {'objects_list': links, 'len': len(links), "pk_topic": pk})


class CreateLink(CreateView):
    model = Interests
    context_object_name = "form"
    fields = ['title', 'url', 'description', 'topic']
    template_name = "interestss/link_create.html"
    success_url = reverse_lazy("topics")

    def get(self, request, *args, **kwargs):
        form = CreateLinkForm()
        return render(request, 'interestss/link_create.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            Interests.objects.create(
                title=form['title'].data,
                url=form['url'].data,
                description=form['description'].data,
                creation_date=datetime.datetime.now(),
                topic=Topic.objects.get(pk=form['topic'].data)
            ).save()
            return redirect("topics")
        return redirect('link-create')


class DeleteLink(DeleteView):
    model = Interests
    context_object_name = "link"
    template_name = "interestss/link_delete.html"
    success_url = reverse_lazy("topics")

    def get(self, request, *args, **kwargs):
        search_pk = self.kwargs.get("pk_link")
        search_topic_pk = self.kwargs.get('pk_topic')
        if search_pk is not None and search_topic_pk is not None:
            request.session['searched'] = request.session['browser_load']
        pk_link = self.kwargs.get("link_pk")
        link = Interests.objects.get(pk=pk_link)
        return render(request, 'interestss/link_delete.html', {'link': link})

    def post(self, request, *args, **kwargs):
        pk_link = self.kwargs.get('link_pk')
        link = Interests.objects.get(pk=pk_link)
        link.delete()
        return redirect('interests-view', pk=self.kwargs.get('topic_pk'))


class UpdateLink(UpdateView):
    model = Interests
    context_object_name = "link"
    template_name = "interestss/link_update.html"
    success_url = reverse_lazy("topics")
    fields = ['title', 'url', 'description', 'topic']

    def get(self, request, *args, **kwargs):
        search_pk = self.kwargs.get("pk_link")
        search_topic_pk = self.kwargs.get('pk_topic')
        if search_pk is not None and search_topic_pk is not None:
            request.session['searched'] = request.session['browser_load']
        pk = self.kwargs.get('link_pk')
        link = Interests.objects.get(pk=pk)
        form = LinkUpdateFrom(initial={
            'title': link.title,
            'url': link.url,
            'description': link.description,
            'topic': link.topic
        })
        return render(request, 'interestss/link_update.html', {"form": form, "link": link})

    def post(self, request, *args, **kwargs):
        pk = self.kwargs.get('link_pk')
        link = Interests.objects.get(pk=pk)
        form = self.get_form()
        if form.is_valid():
            link.title = form['title'].data
            link.url = form['url'].data
            link.description = form['description'].data
            link.update_time = datetime.datetime.now()
            link.topic = Topic.objects.get(pk=form['topic'].data)
            link.save()
            pk_topic = self.kwargs.get('topic_pk')
            return redirect("interests-view", pk=pk_topic)
        pk_topic = self.kwargs.get('topic_pk')
        return redirect('link-update', topic_pk=pk_topic, link_pk=pk)

class DescriptionView(TemplateView):
    model = Interests
    template_name = "interests/description_view.html"

    def get(self, request, *args, **kwargs):
        link_pk = self.kwargs.get('link_pk')
        link = Interests.objects.get(pk=link_pk)
        topic_pk = self.kwargs.get("topic_pk")
        return render(request, 'interestss/description_view.html', {'link': link, })


def load_browser(request, srch, load=False):
    words = srch.split()
    links = []
    for word in words:
        links.extend(Interests.objects.filter(Q(title__icontains=word)))
    links = set(links)
    links = list(links)
    page = request.GET.get('page')
    paginator = Paginator(links, 10)
    try:
        links = paginator.page(page)
    except PageNotAnInteger:
        links = paginator.page(1)
    except EmptyPage:
        links = paginator.page(paginator.num_pages)
    if load:
        return render(request, 'interestss/search_view.html', {'objects_list': links, 'len': len(links), 'q': srch})
    else:
        request.session['browser_load'] = srch
        del request.session['src']
        del request.session['search']
        return render(request, 'interestss/search_view.html', {'objects_list': links, 'len': len(links), 'q': srch})


def search_engine(request, *args, **kwargs):
    if request.method == 'POST':
        request.session['search'] = request.POST.get('q')
        request.session['src'] = True
        return redirect('search-results')


def search_302(request, *args, **kwargs):
    if request.method == "GET":
        if request.session.get('src') is not None:
            srch = request.session['search']
            return load_browser(request, srch)
        else:
            srch = request.session.get('browser_load')
            load = True
            return load_browser(request, srch, load)
