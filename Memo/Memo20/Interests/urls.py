from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('<int:pk>/', views.InterestsView.as_view(), name="interests-view"),
    path('create', views.CreateLink.as_view(), name="link-create"),
    path('delete/<int:topic_pk>/<int:link_pk>/', views.DeleteLink.as_view(), name="link-delete"),
    path('update/<int:link_pk>/<int:topic_pk>/', views.UpdateLink.as_view(), name="link-update"),
    path('delete/<int:topic_pk>/<int:link_pk>/<int:pk_topic><int:pk_link>/', views.DeleteLink.as_view(),
         name="link-delete"),
    path('update/<int:link_pk>/<int:topic_pk>/<int:pk_topic><int:pk_link>/', views.UpdateLink.as_view(),
         name="link-update"),
    path('view/description/<int:topic_pk>/<int:link_pk>/', views.DescriptionView.as_view(), name="description-view"),
    path('search', views.search_engine, name='search-engine'),
    path('search/results', views.search_302, name="search-results")
]
